class Solution:
    def strongPasswordChecker(self, password: str) -> int:
        length = len(password)  # Calcola la lunghezza della password
        missing = 0  # Contatore per i tipi di caratteri mancanti
        missing += 0 if any([i.isdigit() for i in password]) else 1  # Verifica se ci sono numeri nella password
        missing += 0 if any([i.isupper() for i in password]) else 1  # Verifica se ci sono caratteri maiuscoli nella password
        missing += 0 if any([i.islower() for i in password]) else 1  # Verifica se ci sono caratteri minuscoli nella password

        repeat = []  # Lista per memorizzare le lunghezze dei gruppi di caratteri consecutivi
        i, n = 0, 0
        while True:
            while i + n < length:
                if password[i] != password[i + n]:  # Verifica se il carattere successivo è diverso
                    break
                else:
                    n += 1
            i += n
            if n > 2:
                repeat.append(n)  # Aggiunge la lunghezza del gruppo alla lista
            if i >= length:
                break
            n = 0

        repeat_a = sum([i // 3 for i in repeat])  # Calcola il numero di gruppi che possono essere ridotti a lunghezza 2
        repeat_b = [(i + 1) % 3 for i in repeat]  # Calcola i rimanenti gruppi che non possono essere ridotti a lunghezza 2

        if length < 6:
            if missing > 6 - length:  # Se ci sono più tipi di caratteri mancanti del necessario
                return missing
            else:
                return 6 - length  # Aggiunge caratteri fino a raggiungere la lunghezza minima di 6 caratteri

        else:
            if length <= 20:
                if repeat_a > missing:  # Se ci sono più gruppi ripetuti che possono essere ridotti
                    return repeat_a
                else:
                    return missing  # Restituisce il numero minimo tra i tipi di caratteri mancanti e i gruppi ripetuti

            else:
                if repeat_a:  # Se ci sono gruppi ripetuti
                    count_l = length - 20
                    repeat_b.sort()  # Ordina la lista dei rimanenti gruppi
                    for i in repeat_b:
                        if count_l >= i:
                            count_l -= i
                            repeat_a -= 1 if i != 0 else 0
                        else:
                            break
                    repeat_a -= count_l // 3
                    if missing > repeat_a:  # Se ci sono più tipi di caratteri mancanti del necessario
                        return length - 20 + missing
                    else:
                        return length - 20 + repeat_a

                else:
                    return missing + length - 20  # Restituisce la somma tra i tipi di caratteri mancanti e la differenza tra la lunghezza attuale e 20


def main():
    solution = Solution()
    password = input("Inserisci la password: ")
    steps = solution.strongPasswordChecker(password)
    print("Passi necessari:", steps)


if __name__ == "__main__":
    main()
